﻿using System;
using Xamarin.Facebook;
using Xamarin.Facebook.Login;

namespace YoBro.Core.Helpers
{
    public class UserProfileTracker : ProfileTracker
    {
        public event EventHandler<OnProfileChangedEventArgs> OnProfileChanged;

        protected override void OnCurrentProfileChanged(Profile oldProfile, Profile currentProfile)
        {
            if (OnProfileChanged != null)
            {
                OnProfileChanged.Invoke(this, new OnProfileChangedEventArgs(currentProfile));
            }
        }
    }
    public class OnProfileChangedEventArgs : EventArgs
    {
        public Profile mProfile;
        public OnProfileChangedEventArgs(Profile profile)
        {
            mProfile = profile;
        }
    }
}
