using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using YoBro.Core.Models;
using YoBro.Core.Services;
using Firebase.Firestore;
using Firebase;
using Java.Util;

namespace YoBro.Droid.DatabaseAccess
{
    public class DatabaseService : IDatabaseService
    {
        private const string USER_ID = "UserId";
        private const string FIRST_NAME = "FirstName";
        private const string LAST_NAME = "LastName";
        private const string PHONE_NUMBER = "PhoneNumber";
        private const string EMAIL = "Email";
        private Activity _activity;
        FirebaseFirestore database;
        FirebaseApp app;
        readonly string projectID = "yobro-4094d";

        public void Init(object myActivity)
        {
            _activity = (Activity)myActivity;
            var options = new FirebaseOptions.Builder()
                 .SetApplicationId("yobro-4094d")
                 .SetApiKey("AIzaSyCCCNdxmTKYCq76w7AsUAX3WGpLQ1sMz0o")
                 .SetDatabaseUrl("https://" + projectID + ".firebaseio.com")
                 .SetStorageBucket("yobro-4094d.appspot.com")
                 .SetProjectId(projectID)
                 .Build();
            app = FirebaseApp.InitializeApp(_activity, options, projectID);
            database = GetDatabase();
        }

        public FirebaseFirestore GetDatabase()
        {
            FirebaseFirestore database;
            database = FirebaseFirestore.GetInstance(app);
            return database;
        }

        public void SaveProfile(UserProfile userUserProfile)
        {
            HashMap profile = new HashMap();
            profile.Put(USER_ID, userUserProfile.UserId);
            profile.Put(FIRST_NAME, userUserProfile.FirstName);
            profile.Put(LAST_NAME, userUserProfile.LastName);
            profile.Put(PHONE_NUMBER, userUserProfile.PhoneNumber);
            profile.Put(EMAIL, userUserProfile.Email);
            database.Collection("Profiles")
                .Document(userUserProfile.UserId).Set(profile);
        }
    }
}