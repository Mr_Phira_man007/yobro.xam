﻿using MvvmCross.ViewModels;
using YoBro.Core.Services;
using YoBro.Droid.PlatformServices;
using YoBro.Droid.DatabaseAccess;
using MvvmCross;
using MvvmCross.Platforms.Android.Core;
using YoBro.Core;
using MvvmCross.IoC;
using Android.App;
using Android.Content;
using YoBro.Droid.Views;

namespace YoBro.Droid
{
    public class Setup : MvxAndroidSetup<App>
    {

        public Setup() : base()
        {
        }

        protected override IMvxApplication CreateApp()
        {
            return new Core.App();
        }

        protected override void InitializeIoC()
        {
            base.InitializeIoC();
            Mvx.IoCProvider.RegisterSingleton<IFacebookService>(new FacebookService());
            Mvx.IoCProvider.RegisterSingleton<IGoogleService>(new GoogleService());
            Mvx.IoCProvider.RegisterSingleton<IPhoneAuthService>(new PhoneAuthService());
            Mvx.IoCProvider.RegisterSingleton<IDatabaseService>(new DatabaseService());
        }
    }
}