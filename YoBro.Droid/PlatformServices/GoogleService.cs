using System;
using Android.App;
using Android.Content;
using Android.Gms.Auth.Api;
using Android.Gms.Auth.Api.SignIn;
using Android.Gms.Common;
using Android.Gms.Common.Apis;
using Android.OS;
using Firebase;
using Firebase.Auth;
using YoBro.Core.Models;
using YoBro.Core.Services;

namespace YoBro.Droid.PlatformServices
{
    class GoogleService : Java.Lang.Object, IGoogleService, GoogleApiClient.IConnectionCallbacks, GoogleApiClient.IOnConnectionFailedListener
    {   
        private FirebaseAuth mAuth;
        private Activity _activity;
        public int SIGN_IN_ID = 9001;
        private GoogleApiClient mGoogleClient;
        private UserProfile _userProfile;
        private GoogleSignInResult result;

        public void Init(object myActivity, UserProfile profile)
        {
            _activity = (Activity)myActivity;
            _userProfile = profile;
            FirebaseApp.InitializeApp(_activity);
            mAuth = FirebaseAuth.Instance; 
            if (mGoogleClient == null)
            {
                GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DefaultSignIn)
                .RequestIdToken("201755256317-qnna1g72r69c30i4gt5h7ojtuam4k6fp.apps.googleusercontent.com")
                .RequestProfile()
                .RequestEmail()
                .Build();

                mGoogleClient = new GoogleApiClient.Builder(_activity.ApplicationContext)
                .AddApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)
                .AddConnectionCallbacks(this)
                .Build();
            }
           
        }

        public void SignIn()
        {
            var signInIntend = Auth.GoogleSignInApi.GetSignInIntent(mGoogleClient);
            _activity.StartActivityForResult(signInIntend, SIGN_IN_ID);           
        }

        public void FinalizeSigin(int requestCode, int resultCode, object data)
        {
            if (requestCode == SIGN_IN_ID)
            {
                result = Auth.GoogleSignInApi.GetSignInResultFromIntent((Intent)data);
                if (result.IsSuccess)
                {
                    HandleSignInResult(result);
                } else
                {
                    HandleSignInFailed(result);
                }
            }
        }

        private void HandleSignInFailed(GoogleSignInResult result)
        {

        }

        private void HandleSignInResult(GoogleSignInResult result)
        {
            try
            {
                AuthCredential credential = GoogleAuthProvider.GetCredential(result.SignInAccount.IdToken, null);
                mAuth.SignInWithCredential(credential);
                GoogleSignInAccount account = result.SignInAccount;
                _userProfile.FirstName = account.GivenName;
                _userProfile.LastName = account.FamilyName;
                _userProfile.Email = account.Email;
                _userProfile.UserId = mAuth.CurrentUser.Uid;
                _userProfile.IsLoggedIn = true;
            }
            catch (Exception)
            {

            }
        }

        public void LogOut()
        {
            
        }

        public void OnConnected(Bundle connectionHint)
        {
           
        }

        public void OnConnectionSuspended(int cause)
        {
           
        }

        public void OnConnectionFailed(ConnectionResult result)
        {
            
        }

        public UserProfile GetProfile()
        {
            return _userProfile;
        }
    }
}