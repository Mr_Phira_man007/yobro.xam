using System;
using Android.App;
using Android.Content;
using Android.Gms.Auth.Api;
using Android.Gms.Auth.Api.SignIn;
using Android.Gms.Common;
using Android.Gms.Common.Apis;
using Android.Gms.Tasks;

using Android.OS;
using Android.Widget;
using Firebase;
using Firebase.Auth;
using Java.Util.Concurrent;
using YoBro.Core.Models;
using YoBro.Core.Services;
using static Firebase.Auth.PhoneAuthProvider;
using Pattern = Java.Util.Regex.Pattern;

namespace YoBro.Droid.PlatformServices
{
    public class PhoneAuthService : OnVerificationStateChangedCallbacks, IOnCompleteListener, IPhoneAuthService
    {
        private bool _isValidForPhone = false;
        private bool _isValidForCode = false;
        private FirebaseAuth _auth;
        private Activity _activity;
        private UserProfile _userProfile;
        private string _verificationId;
        private ForceResendingToken _token;
        private static string regex = @"0((60[3-9]|64[0-5]|66[0-5])\d{6}|(7[1-4689]|6[1-9]|8[1-4])\d{7})";
        private const string COUTNRY_CODE = "+27";
        private string _phoneNum;
        private string _code;
        private string _tempNo;

        public void Init(object myActivity, UserProfile profile)
        {
            _activity = (Activity)myActivity;
            FirebaseApp.InitializeApp(_activity);
            _auth = FirebaseAuth.Instance;
            _userProfile = profile;
        }

        public void OnComplete(Task task)
        {
            if (task.IsSuccessful)
            {
                FirebaseUser user = _auth.CurrentUser;
                _userProfile.PhoneNumber = _auth.CurrentUser.PhoneNumber;
                _userProfile.UserId = _auth.CurrentUser.Uid;
                _userProfile.IsLoggedIn = true;

            }
        }

        public override void OnVerificationCompleted(PhoneAuthCredential credential)
        {
            SignInWithPhoneAuthCredential(credential);
        }

        private void SignInWithPhoneAuthCredential(PhoneAuthCredential credential)
        {
            _auth.SignInWithCredential(credential)
                .AddOnCompleteListener(this);
        }

        public override void OnVerificationFailed(FirebaseException exception)
        {
            string msg = exception.Message;
        }

        public override void OnCodeSent(string verificationId, ForceResendingToken forceResendingToken)
        {
            base.OnCodeSent(verificationId, forceResendingToken);
            _verificationId = verificationId;
            _token = forceResendingToken;
        }

        public void Verify()
        {
            PhoneAuthCredential credential = PhoneAuthProvider.GetCredential(_verificationId, _code);
            SignInWithPhoneAuthCredential(credential);
        }

        public bool ValidatePhoneNumber(string phoneNo)
        {
            _tempNo = phoneNo;
            if (Pattern.Matches(regex, phoneNo) && phoneNo.Length == 10)
            {
                _isValidForPhone = true;
                phoneNo = phoneNo.Remove(0, 1);
                _phoneNum = phoneNo.Insert(0, COUTNRY_CODE);
                AlertDialog.Builder dialog = new AlertDialog.Builder(_activity);
                AlertDialog alert = dialog.Create();
                alert.SetMessage("You are about to send an OTP to the following number " + _tempNo + ". Is this correct?");
                alert.SetButton("YES", (c, ev) =>
                {
                    PhoneAuthProvider.GetInstance(_auth)
                    .VerifyPhoneNumber(
                        _phoneNum,
                        2,
                        TimeUnit.Minutes,
                        _activity,
                        this);
                });
                alert.SetButton2("NO", (c, ev) => { });
                alert.Show();
            }
            if (!Pattern.Matches(regex, phoneNo) && phoneNo.Length == 10)
            {
                _isValidForPhone = false;
                AlertDialog.Builder dialog = new AlertDialog.Builder(_activity);
                AlertDialog alert = dialog.Create();
                alert.SetMessage("Phone number entered is in a wrong format.");
                alert.SetButton("OK", (c, ev) =>
                {
                });
                alert.Show();
            }

            return _isValidForPhone;
        }

        public bool ValidateCode(string code)
        {
            _code = code;
            if (_code.Length == 6)
                _isValidForCode = true;

            return _isValidForCode;
        }

        public UserProfile GetProfile()
        {
            return _userProfile;
        }
    }
}