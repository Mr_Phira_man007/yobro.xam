﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using YoBro.Core.Services;
using Xamarin.Facebook;
using Android.Gms.Tasks;
using Xamarin.Facebook.Login;
using Firebase;
using Firebase.Auth;
using YoBro.Core.Helpers;
using YoBro.Core.Models;
using Org.Json;
using Android.OS;

namespace YoBro.Droid.PlatformServices
{
    public class FacebookService : Java.Lang.Object, IFacebookService, IFacebookCallback, IOnCompleteListener, GraphRequest.IGraphJSONObjectCallback
    {
        public ICallbackManager mCallbackManager;
        private FirebaseAuth mAuth;
        private Activity _context;
        private UserProfile _userProfile;
        private AccessToken _accessToken;

        public void Init(object myActivity, UserProfile profile)
        {
            _context = (Activity)myActivity;
            _userProfile = profile;
            FirebaseApp.InitializeApp(_context.ApplicationContext);
            mAuth = FirebaseAuth.Instance;
            mCallbackManager = CallbackManagerFactory.Create();
        }

        public void SignIn()
        {
            LoginManager.Instance.LogInWithReadPermissions(_context, new List<string> { "email", "public_profile" });
            LoginManager.Instance.RegisterCallback(mCallbackManager, this);
        }

        public void LogOut()
        {
            LoginManager.Instance.LogOut();
        }

        private void HandleFacebookAccessToken(AccessToken accessToken)
        {
            _accessToken = accessToken;
            AuthCredential credential = FacebookAuthProvider.GetCredential(accessToken.Token);
            mAuth.SignInWithCredential(credential).AddOnCompleteListener(_context, this);
        }

        public void OnComplete(Task task)
        {
            if (task.IsSuccessful)
            {
                GraphRequest request = GraphRequest.NewMeRequest(_accessToken, this);
                Bundle parameters = new Bundle();
                parameters.PutString("fields", "first_name,last_name");
                request.Parameters = parameters;
                request.ExecuteAsync();
                _userProfile.PhoneNumber = mAuth.CurrentUser.PhoneNumber;
                _userProfile.Email = mAuth.CurrentUser.Email;
                _userProfile.UserId = mAuth.CurrentUser.Uid;
            }
        }

        public void OnError(FacebookException error)
        {

        }

        public void OnSuccess(Java.Lang.Object result)
        {
            LoginResult loginResult = result as LoginResult;
            HandleFacebookAccessToken(loginResult.AccessToken);
        }

        public void FinalizeSigin(int requestCode, int resultCode, object data)
        {
            mCallbackManager.OnActivityResult(requestCode, resultCode, (Intent)data);
        }

        public void OnCancel()
        {

        }

        public UserProfile GetProfile()
        {
            return _userProfile;
        }

        public void OnCompleted(JSONObject json, GraphResponse response)
        {
            _userProfile.FirstName = json.GetString("first_name");
            _userProfile.LastName = json.GetString("last_name");
            _userProfile.IsLoggedIn = true;
        }
    }
}