
using Android.App;
using Android.Widget;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Droid.Support.V7.AppCompat;
using YoBro.Core.ViewModels;

namespace YoBro.Droid.Views
{
    [Activity(Label = "YoBro", MainLauncher = true, Theme = "@style/AppTheme")]
    public class ProfileView : MvxAppCompatActivity<ProfileViewModel>
    {

        protected override void OnViewModelSet()
        {
            base.OnViewModelSet();
            SetContentView(Resource.Layout.profile_view);
            ViewModel.InitService(this);


            var tv_heading = FindViewById<TextView>(Resource.Id.txtHeading);
            var et_emailPhoneHint = FindViewById<Android.Support.Design.Widget.TextInputLayout>(Resource.Id.textInputLayoutEmail);
            var et_emailPhone = FindViewById<EditText>(Resource.Id.txtEmailPhone);
            var etFirstName = FindViewById<EditText>(Resource.Id.txtFirstName);
            var etLastName = FindViewById<EditText>(Resource.Id.txtLN);

            var setFirstName = this.CreateBindingSet<ProfileView, ProfileViewModel>();
            setFirstName.Bind(etFirstName).To(vm => vm.Firstname);
            setFirstName.Apply();
            var setLastName = this.CreateBindingSet<ProfileView, ProfileViewModel>();
            setLastName.Bind(etLastName).To(vm => vm.LastName);
            setLastName.Apply();
            if (ViewModel.PhoneNo == null)
            {
                tv_heading.Text = "Yo " + ViewModel.Firstname + ", Please confirm your details below are \ncorrect.";
                et_emailPhoneHint.Hint = "Email";
                var set = this.CreateBindingSet<ProfileView, ProfileViewModel>();
                set.Bind(et_emailPhone).To(vm => vm.Email);
                set.Apply();
            }
            else if(ViewModel.Email == null)
            {
                tv_heading.Text = "Please enter your details below to setup your basic profile.";
                et_emailPhoneHint.Hint = "Phone Number";
                var set = this.CreateBindingSet<ProfileView, ProfileViewModel>();
                set.Bind(et_emailPhone).To(vm => vm.PhoneNo);
                set.Apply();
            }
           
        }
    }
}