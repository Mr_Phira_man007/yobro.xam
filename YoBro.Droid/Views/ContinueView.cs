using Android.App;
using Android.Content;
using YoBro.Core.ViewModels;
using MvvmCross.Droid.Support.V7.AppCompat;
using Android.Widget;

namespace YoBro.Droid.Views
{
    [Activity(Label = "YoBro", MainLauncher = false, Theme = "@style/AppTheme")]
    public class ContinueView : MvxAppCompatActivity<ContinueViewModel>
    {
        protected override void OnViewModelSet()
        {
            base.OnViewModelSet();
            SetContentView(Resource.Layout.welcomeView);
            var tv_WelcomeMessage = FindViewById<TextView>(Resource.Id.tvWelcomeMsg);
            tv_WelcomeMessage.Text = "Yo " + ViewModel.Firstname + ", thank you for signing up. \nPlease continue.";
        }
    }
}