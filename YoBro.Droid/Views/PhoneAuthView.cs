﻿using Android.App;
using Android.Content;
using YoBro.Core.ViewModels;
using MvvmCross.Droid.Support.V7.AppCompat;

namespace YoBro.Droid.Views
{
    [Activity(Label = "YoBro", MainLauncher = false, Theme = "@style/AppTheme")]
    public class PhoneAuthView : MvxAppCompatActivity<PhoneAuthViewModel>
    {

        protected override void OnViewModelSet()
        {
            base.OnViewModelSet();
            SetContentView(Resource.Layout.phoneauth_view);
            ViewModel.InitService(this);
        }
    }
}