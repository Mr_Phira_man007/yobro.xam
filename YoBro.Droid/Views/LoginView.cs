
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using YoBro.Core.ViewModels;
using MvvmCross.Droid.Support.V7.AppCompat;

namespace YoBro.Droid.Views
{
    [Activity(Label = "YoBro", MainLauncher = true, Theme = "@style/AppTheme")]
    public class LoginView : MvxAppCompatActivity<LoginViewModel>
    {

        protected override void OnViewModelSet()
        {
            base.OnViewModelSet();
            SetContentView(Resource.Layout.activity_main);
            ViewModel.InitService(this);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            ViewModel.SignInRequestCode = requestCode;
            ViewModel.SignResultsCode = (int)resultCode;
            ViewModel.signInData = data;
            ViewModel.IsLoginComplete = true;
        }    
    }
}