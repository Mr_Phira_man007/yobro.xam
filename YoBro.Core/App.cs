﻿using System;
using System.Collections.Generic;
using System.Text;
using MvvmCross;
using MvvmCross.ViewModels;
using YoBro.Core.ViewModels;
using YoBro.Core.Services;
using MvvmCross.IoC;

namespace YoBro.Core
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();
            RegisterAppStart<LoginViewModel>();
        }
    }
}
