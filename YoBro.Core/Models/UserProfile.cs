﻿using System;
using System.Collections.Generic;
using System.Text;
using MvvmCross.ViewModels;
using System.ComponentModel;

namespace YoBro.Core.Models
{
    public class UserProfile: IYobroObservable
    {
        private bool _isLoggedIn = false;

        public string UserId;
        public string Email;
        public string PhoneNumber;
        public string FirstName;
        public string LastName;
        public IYobroObserver Observer;

        public bool IsLoggedIn 
        {   get {return _isLoggedIn;}
            set
            {
                _isLoggedIn = value;
                NotifyAll();
            }
        }

        public void NotifyAll()
        {
            if (Observer != null)
            {
                Observer.onValueChanged();
            }
        }
    }
}