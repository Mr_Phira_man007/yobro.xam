﻿namespace YoBro.Core.Models
{
    public interface IYobroObservable
    {
        void NotifyAll();
    }
}