﻿namespace YoBro.Core.Models
{
    public interface IYobroObserver
    {
        void onValueChanged();
    }
}