﻿using YoBro.Core.Models;

namespace YoBro.Core.Services
{
    public interface IPhoneAuthService
    {
        void Verify();
        bool ValidatePhoneNumber(string phoneNo);
        bool ValidateCode(string code);
        void Init(object data, UserProfile profile);
        UserProfile GetProfile();
    }
}
