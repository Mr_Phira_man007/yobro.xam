﻿using System;
using System.Collections.Generic;
using System.Text;
using YoBro.Core.Models;

namespace YoBro.Core.Services
{
    public interface ILoginService
    {
        void SignIn();
        void LogOut();
        void FinalizeSigin(int requestCode, int resultCode, object data);
        void Init(object data, UserProfile profile);
        UserProfile GetProfile();
    }
}
