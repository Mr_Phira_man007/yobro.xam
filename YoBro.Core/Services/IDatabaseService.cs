using System;
using System.Collections.Generic;
using System.Text;
using YoBro.Core.Models;

namespace YoBro.Core.Services
{
    public interface IDatabaseService
    {
        void Init(object data);
        void SaveProfile(UserProfile userProfile);
    }
}
