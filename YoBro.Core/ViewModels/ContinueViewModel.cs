using System;
using MvvmCross.ViewModels;
using MvvmCross.Navigation;
using MvvmCross.Commands;
using YoBro.Core.Services;
using YoBro.Core.Models;
using System.ComponentModel;

namespace YoBro.Core.ViewModels
{
    public class ContinueViewModel : MvxViewModel
    {
        private readonly IMvxNavigationService _navigationService;
        private readonly IFacebookService _facebookService;
        private readonly IGoogleService _googleService;
        private readonly IPhoneAuthService _phoneAuthService;
        public UserProfile UserProfile;

        public ContinueViewModel(IFacebookService facebookService,
            IMvxNavigationService navigationService, IGoogleService googleService, IPhoneAuthService phoneAuthService)
        {
            _navigationService = navigationService;
            _facebookService = facebookService;
            _googleService = googleService;
            _phoneAuthService = phoneAuthService;
            ContinueCommand = new MvxCommand(DoMyContinueCommand);
            UserProfile = _facebookService.GetProfile().UserId != null ?
        _facebookService.GetProfile() : _googleService.GetProfile().UserId != null ?
        _googleService.GetProfile() : _phoneAuthService.GetProfile();

            Firstname = UserProfile.FirstName;
        }
        public string Firstname { get; set; }

        public MvxCommand ContinueCommand { get; private set; }
        private void DoMyContinueCommand()
        {

        }
    }
}
