﻿using System;
using MvvmCross.ViewModels;
using YoBro.Core.Models;
using MvvmCross.Navigation;
using MvvmCross.Commands;
using YoBro.Core.Services;
using System.Threading.Tasks;
using System.ComponentModel;

namespace YoBro.Core.ViewModels
{
    public class LoginViewModel : MvxViewModel, IYobroObserver
    {
        private readonly IFacebookService _facebookService;
        private readonly IGoogleService _googleService;
        private readonly IMvxNavigationService _navigationService;
        private bool _isLoginComplete = false;
        private bool _isLoginSuccessful;

        public UserProfile _userProfile;
        public SiginInType SiginInType;
        public object signInData;
        public int SignInRequestCode;
        public int SignResultsCode;

        public bool IsLoginSuccessful
        {
            get => _isLoginSuccessful;
            set => SetProperty(ref _isLoginSuccessful, value);
        }

        public bool IsLoginComplete
        {
            set
            {
                _isLoginComplete = value;
                if (_isLoginComplete)
                {
                    if (SiginInType == SiginInType.GOOGLE)
                    {
                        _googleService.FinalizeSigin(SignInRequestCode, SignResultsCode, signInData);
                    }
                    else if (SiginInType == SiginInType.FACEBOOK)
                    {
                        _facebookService.FinalizeSigin(SignInRequestCode, SignResultsCode, signInData);
                    }
                }
            }
            get
            {
                return _isLoginComplete;
            }
        }

        public LoginViewModel(IFacebookService facebookService, IGoogleService googleService, IMvxNavigationService navigationService)
        {
            _facebookService = facebookService;
            _googleService = googleService;
            _navigationService = navigationService;
            FacebookLoginCommand = new MvxCommand(DoMyFacebookLoginCommand);
            GoogleLoginCommand = new MvxCommand(DoMyGoogleLoginCommand);
            PhoneLoginCommand = new MvxCommand(DoMyPhoneLoginCommand);
            _userProfile = new UserProfile();
            _userProfile.Observer = this;
        }

        public void InitService(object data)
        {
            _googleService.Init(data, _userProfile);
            _facebookService.Init(data, _userProfile);
        }

        public MvxCommand FacebookLoginCommand { get; private set; }
        private void DoMyFacebookLoginCommand()
        {
            SiginInType = SiginInType.FACEBOOK;
            _facebookService.SignIn();
        }

        public MvxCommand GoogleLoginCommand { get; private set; }
        private void DoMyGoogleLoginCommand()
        {
            SiginInType = SiginInType.GOOGLE;
            _googleService.SignIn();
        }

        public MvxCommand PhoneLoginCommand { get; private set; }
        private void DoMyPhoneLoginCommand()
        {
            SiginInType = SiginInType.PHONE;
            _navigationService.Navigate<PhoneAuthViewModel>();
        }

        public MvxCommand LogOutCommand { get; private set; }
        private void DoMyLogOutCommand()
        {
            _facebookService.LogOut();
        }

        public void onValueChanged()
        {
            _navigationService.Navigate<ProfileViewModel>();
        }
    }
}
