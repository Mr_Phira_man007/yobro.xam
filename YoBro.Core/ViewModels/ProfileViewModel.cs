using System;
using MvvmCross.ViewModels;
using MvvmCross.Navigation;
using MvvmCross.Commands;
using YoBro.Core.Services;
using YoBro.Core.Models;
using System.ComponentModel;

namespace YoBro.Core.ViewModels
{
    public class ProfileViewModel : MvxViewModel
    {
        private readonly IMvxNavigationService _navigationService;
        private readonly IDatabaseService _databaseService;
        private readonly IFacebookService _facebookService;
        private readonly IGoogleService _googleService;
        private readonly IPhoneAuthService _phoneAuthService;
        private bool isDoneWriting = false;
        public UserProfile UserProfile;
        private string _userId;

        public ProfileViewModel(IDatabaseService databaseService, IFacebookService facebookService,
            IMvxNavigationService navigationService, IGoogleService googleService, IPhoneAuthService phoneAuthService)
        {
            _databaseService = databaseService;
            _navigationService = navigationService;
            _facebookService = facebookService;
            _googleService = googleService;
            _phoneAuthService = phoneAuthService;
            SaveProfileCommand = new MvxCommand(DoMySaveProfileCommand);
        }

        public void InitService(object data)
        {
            UserProfile = _facebookService.GetProfile().UserId != null ?
                _facebookService.GetProfile() : _googleService.GetProfile().UserId != null ?
                _googleService.GetProfile() : _phoneAuthService.GetProfile();
            _userId = UserProfile.UserId;
            Firstname = UserProfile.FirstName;
            LastName = UserProfile.LastName;
            PhoneNo = UserProfile.PhoneNumber;
            Email = UserProfile.Email;
            _databaseService.Init(data);
        }

        private string _phoneNo;
        public string PhoneNo
        {
            get { return _phoneNo; }
            set
            {
                _phoneNo = value;
                UserProfile.PhoneNumber = PhoneNo;
                RaisePropertyChanged(() => PhoneNo);
            }
        }

        private string _firstname;
        public string Firstname
        {
            get
            {
                return _firstname;
            }
            set
            {
                _firstname = value;
                UserProfile.FirstName = Firstname;
                RaisePropertyChanged(() => Firstname);
            }
        }

        private string _lastname;
        public string LastName
        {
            get { return _lastname; }
            set
            {
                _lastname = value;
                UserProfile.LastName = LastName;
                RaisePropertyChanged(() => LastName);
            }
        }

        private string _email;
        public string Email
        {
            get { return _email; }
            set
            {
                _email = value;
                UserProfile.Email = Email;
                RaisePropertyChanged(() => Email);
            }
        }

        public MvxCommand SaveProfileCommand { get; private set; }
        private void DoMySaveProfileCommand()
        {
            _databaseService.SaveProfile(UserProfile);
            _navigationService.Navigate<ContinueViewModel>();
        }
    }
}
