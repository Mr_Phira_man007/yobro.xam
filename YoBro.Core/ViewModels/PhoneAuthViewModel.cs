using System;
using MvvmCross.ViewModels;
using MvvmCross.Navigation;
using MvvmCross.Commands;
using YoBro.Core.Services;
using YoBro.Core.Models;

namespace YoBro.Core.ViewModels
{
    public class PhoneAuthViewModel : MvxViewModel, IYobroObserver
    {
        private readonly IMvxNavigationService _navigationService;
        private readonly IPhoneAuthService _phoneAuthService;
        private bool _isPhoneValid;
        private bool _isCodeValid;
        public UserProfile _userProfile;

        public PhoneAuthViewModel(IPhoneAuthService phoneAuthService, IMvxNavigationService navigationService)
        {
            _phoneAuthService = phoneAuthService;
            _navigationService = navigationService;
            PhoneAuthVerifyCommand = new MvxCommand(DoMyPhoneAuthCommand, MyCommandCanExecute);
            _userProfile = new UserProfile();
            _userProfile.Observer = this;
        }

        public void InitService(object data)
        {
            _phoneAuthService.Init(data, _userProfile);
        }

        private string _phoneNo;
        public string PhoneNo
        {
            get { return _phoneNo; }
            set
            {
                SetProperty(ref _phoneNo, value);
                _isPhoneValid = IsValidPhoneNumber();
                PhoneAuthVerifyCommand.RaiseCanExecuteChanged();
            }
        }

        private string _code;
        public string Code
        {
            get { return _code; }
            set
            {
                SetProperty(ref _code, value);
                _isCodeValid = IsValidCode();
                PhoneAuthVerifyCommand.RaiseCanExecuteChanged();
            }
        }

        public bool IsValidPhoneNumber()
        {
            return _phoneAuthService.ValidatePhoneNumber(PhoneNo);
        }

        public bool IsValidCode()
        {
            return _phoneAuthService.ValidateCode(Code);
        }

        public bool MyCommandCanExecute()
        {

            return (!string.IsNullOrEmpty(PhoneNo) || !string.IsNullOrEmpty(Code)) && (_isPhoneValid && _isCodeValid);
        }

        public MvxCommand PhoneAuthVerifyCommand { get; private set; }
        private void DoMyPhoneAuthCommand()
        {
            _phoneAuthService.Verify();
        }

        public void onValueChanged()
        {
            _navigationService.Navigate<ProfileViewModel>();
        }
    }
}
